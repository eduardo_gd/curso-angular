var usuarios = [];

function consultarUsuarios() {
    var promesa = new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve(usuarios);
        }, 3000);
    });

    return promesa;
}

function guardarUsuario(usuario) {
    usuarios.push(usuario);

    var promesa = new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve();
        }, 3000);
    });

    return promesa;
}

function eliminarUsuario(id) {
    usuarios = usuarios.filter(usuario => usuario.id != id);

    var promesa = new Promise(function (resolve, reject) {
        setTimeout(function () {
            if (!usuarios.length) reject();
            else resolve();
        }, 3000);
    });

    return promesa;
}

/* -------------------------------- */

console.log("Ejecutando Webservices");

consultarUsuarios()
    .then(function (listaUsuarios) {
        console.log("Usuarios en BD", listaUsuarios);
        return guardarUsuario({ nombre: "Jorge", id: 1 });
    })
    .then(function () {
        console.log("El usuario Jorge ya se guardó");
        return guardarUsuario({ nombre: "Dulce", id: 2 });
    })
    .then(function () {
        console.log("El usuario Dulce ya se guardó");
        return consultarUsuarios();
    })
    .then(function (nuevaLista) {
        console.log("Usuarios en BD", nuevaLista);
        return eliminarUsuario(1);
    })
    .then(function () {
        console.log("El usuario Jorge se eliminó");
        return consultarUsuarios();
    }, function () {
        console.log("Hubo un error al eliminar");
        return Promise.reject();
    })
    .then(function (ultimaLista) {
        console.log("Usuarios en BD", ultimaLista);
    }, function () {
        console.log("Todo esta mal!!")
    });