var promesa = new Promise(function (resolve, reject) {
    setTimeout(function () {
        resolve();
    }, 3000);
});

var promesa2 = new Promise(function (resolve, reject) {
    resolve();
});

promesa
    // caso de éxito
    .then(function () {
        console.log("Todo salio bien");
        return promesa2;
    })
    .then(function () {
        console.log("La promesa 2");
    });