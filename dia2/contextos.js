function Persona(nombre, apP, apM) {
    // público
    this.nombre = nombre;
    this.apellidoPaterno = apP;
    this.apellidoMaterno = apM;

    this.nombreCompleto = function () {
        return `${this.nombre} ${this.apellidoPaterno} ${this.apellidoMaterno}`;
    }

    this.saludo = function () {
        setTimeout(() => {
            console.log("Hola mi nombre es", this.nombre);
        }, 3000)
    }


    // privado
    // private id;
    var id = 0;
    
    this.getId = function () { return id; }


    console.log("ID persona", id)
}

var p = new Persona("Jorge", "Ramon", "Cisneros");
console.log("Es instancia de Persona?", p instanceof Persona)
console.log("Mi nombre es", p.nombreCompleto());

p.saludo();
console.log("Mi ID es", p.getId())