function closureEjemplo() {
    let x = 0;
    return () => console.log(++x);
}

var closure = closureEjemplo();
closure();
closure();
closure();
closure();
closure();
closure();
closure();
closure();
closure();

function variableGlobal() {
    var y = 0;

    function esCeroClosure() {
        let x = y;
        return () => console.log(x);
    }

    var esCero = esCeroClosure();

    var y = 10;

    function esDiezClosure() {
        let x = y;
        return {
            sucesor: () => console.log(x + 1),
            antecesor: () => console.log(x - 1)
        }
    }

    var esDiez = esDiezClosure();

    var y = 100;

    function esCienClosure() {
        let x = y;
        return () => console.log(x);
    }

    var esCien = esCienClosure();

    esCero();
    esDiez.sucesor();
    esDiez.antecesor();
    esCien();
}

variableGlobal();