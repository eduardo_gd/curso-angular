// Javascript es un lenguaje de orden superior

function miFuncion(parametro) {
    parametro();
}

function miFuncion2() {
    return function () {
        console.log("soy una funcion");
    }
}

// Lambdas (Arrow Functions)
// [].each { x,y,z -> println x } Groovy (Closure)
// (a,b,c) => código
let f2 = () => console.log("Soy una función de flecha");
f2();

let f3 = () => {
    let x = 0;
    console.log("X es", x);
    return true;
}

f3();

let f4 = () => true;

console.log("f4 regresa", f4());