var x = 0;

console.log("La variable x es de tipo:", typeof x);

x = true;

console.log("La variable x es de tipo:", typeof x);

var x = "Me vale gorro";

console.log("La variable x es de tipo:", typeof x);

var y = function (parametro1, parametro2) {
    console.log("Recibí", parametro1, parametro2);
}

var resultado = y(0, 1);
console.log("El primer resultado de la función es:", resultado);
console.log("El valor de la función es:", y("hola", "mundo"));

const LLAVE_SECRETA = "oLA K Ase";

let i = 0;

console.log("La constante de la llave secreta es:", LLAVE_SECRETA);
i = null;
console.log("i vale", i);
// Tipos de valores:
// cadenas -> String
// números -> Number
// objetos -> Object
// arreglos -> Array
// funciones -> Function
// null
// undefined
// booleanos -> Boolean